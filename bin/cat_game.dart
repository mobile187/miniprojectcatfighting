import 'dart:io';
import 'package:cat_game/cat_game.dart' as cat_game;
import 'giantCat.dart';
import 'whiteCat.dart';
import 'blackCat.dart';

String currentPlayer = "P1";

class MainCat {
  whiteCat p1cat1 = new whiteCat();
  blackCat p1cat2 = new blackCat();
  giantCat p1cat3 = new giantCat();

  whiteCat p2cat1 = new whiteCat();
  blackCat p2cat2 = new blackCat();
  giantCat p2cat3 = new giantCat();

  whiteCat p3cat1 = new whiteCat();
  blackCat p3cat2 = new blackCat();
  giantCat p3cat3 = new giantCat();

  var name1;
  var name2;

  var hp1;
  var hp2;

  var mana1;
  var mana2;

  var special1;
  var special2;

  var count1 = 0;
  var count2 = 0;

  SelectCat1(String selectP1) {
    if (selectP1 == "1") {
      name1 = p1cat1.getName();
    } else if (selectP1 == "2") {
      name1 = p1cat2.getName();
    } else if (selectP1 == "3") {
      name1 = p1cat3.getName();
    } else {
      print("Please enter the correct number.");
      count1 = -1;
    }
    count1++;
    return name1;
  }

  SelectCat2(String selectP2) {
    if (selectP2 == "1") {
      name2 = p2cat1.getName();
    } else if (selectP2 == "2") {
      name2 = p2cat2.getName();
    } else if (selectP2 == "3") {
      name2 = p2cat3.getName();
    } else {
      print("Please enter the correct number.");
      count2 = -1;
    }
    count2++;
    return name2;
  }

  checkCatHP1(String selectP1) {
    if (selectP1 == "1") {
      hp1 = p1cat1.getHp();
    } else if (selectP1 == "2") {
      hp1 = p1cat2.getHp();
    } else if (selectP1 == "3") {
      hp1 = p1cat3.getHp();
    }
    return hp1;
  }

  checkCatHP2(String selectP2) {
    if (selectP2 == "1") {
      hp2 = p2cat1.getHp();
    } else if (selectP2 == "2") {
      hp2 = p2cat2.getHp();
    } else if (selectP2 == "3") {
      hp2 = p2cat3.getHp();
    }
    return hp2;
  }

  checkCatMN1(String selectP1) {
    if (selectP1 == "1") {
      mana1 = p1cat1.getMana();
    } else if (selectP1 == "2") {
      mana1 = p1cat2.getMana();
    } else if (selectP1 == "3") {
      mana1 = p1cat3.getMana();
    }
    return mana1;
  }

  checkCatMN2(String selectP2) {
    if (selectP2 == "1") {
      mana2 = p2cat1.getMana();
    } else if (selectP2 == "2") {
      mana2 = p2cat2.getMana();
    } else if (selectP2 == "3") {
      mana2 = p2cat3.getMana();
    }
    return mana2;
  }

  checkCatSP1(String selectP1) {
    if (selectP1 == "1") {
      special1 = p1cat1.getSpecial();
    } else if (selectP1 == "2") {
      special1 = p1cat2.getSpecial();
    } else if (selectP1 == "3") {
      special1 = p1cat3.getSpecial();
    }
    return special1;
  }

  checkCatSP2(String selectP2) {
    if (selectP2 == "1") {
      special2 = p2cat1.getSpecial();
    } else if (selectP2 == "2") {
      special2 = p2cat2.getSpecial();
    } else if (selectP2 == "3") {
      special2 = p2cat3.getSpecial();
    }
    return special2;
  }
}

void main(List<String> arguments) {
  MainCat mainCat = MainCat();

  var countH = 0;

  whiteCat p1cat1 = new whiteCat();
  blackCat p1cat2 = new blackCat();
  giantCat p1cat3 = new giantCat();

  whiteCat p2cat1 = new whiteCat();
  blackCat p2cat2 = new blackCat();
  giantCat p2cat3 = new giantCat();

  while (mainCat.count1 == 0 && mainCat.count1 != -1) {
    print("------------------------");
    print("Please select your cat.");
    print("------------------------");
    print("1) ${p1cat1.getName()}");
    print("\u001b[47;1m");
    print("        /\\___/\\         ");
    print("       ( =^.^= )        ");
    print("\u001b[0m");
    print("ATK: 1-3");
    print("HP: 8");
    print("Mana: 1");
    print("Special: Critical Damage!!");
    print("------------------------");

    print("2) ${p1cat2.getName()}");
    print("\u001b[40;1m");
    print("        /\\___/\\         ");
    print("       ( ='.'= )         ");
    print("\u001b[0m");
    print("ATK: 1-3");
    print("HP: 10");
    print("Mana: 1");
    print("Special: Never Miss!!");
    print("------------------------");

    print("3) ${p1cat3.getName()}");
    print("\u001b[41;1m");
    print("       /\\_____/\\        ");
    print("      (  =^o^=  )       ");
    print("\u001b[0m");
    print("ATK: 1-3");
    print("HP: 10");
    print("Mana: 2");
    print("Special: Big Heal!!");
    print("------------------------");

    stdout.write("Player1 Select: ");
    String selectP1 = stdin.readLineSync()!;
    mainCat.SelectCat1(selectP1);
    mainCat.checkCatHP1(selectP1);
    mainCat.checkCatMN1(selectP1);
    mainCat.checkCatSP1(selectP1);
    sleep(const Duration(seconds: 1));
    clearScreen();
  }

  while (mainCat.count2 == 0 && mainCat.count2 != -1) {
    print("------------------------");
    print("Please select your cat.");
    print("------------------------");
    print("1) ${p2cat1.getName()}");
    print("\u001b[47;1m");
    print("        /\\___/\\         ");
    print("       ( =^.^= )        ");
    print("\u001b[0m");
    print("ATK: 1-3");
    print("HP: 8");
    print("Mana: 1");
    print("Special: Critical Damage!!");
    print("------------------------");

    print("2) ${p2cat2.getName()}");
    print("\u001b[40;1m");
    print("        /\\___/\\         ");
    print("       ( ='.'= )         ");
    print("\u001b[0m");
    print("ATK: 1-3");
    print("HP: 10");
    print("Mana: 1");
    print("Special: Never Miss!!");
    print("------------------------");

    print("3) ${p2cat3.getName()}");
    print("\u001b[41;1m");
    print("       /\\_____/\\        ");
    print("      (  =^o^=  )       ");
    print("\u001b[0m");
    print("ATK: 1-3");
    print("HP: 15");
    print("Mana: 2");
    print("Special: Big Heal!!");
    print("------------------------");

    stdout.write("Player2 Select: ");
    String selectP2 = stdin.readLineSync()!;
    mainCat.SelectCat2(selectP2);
    mainCat.checkCatHP2(selectP2);
    mainCat.checkCatMN2(selectP2);
    mainCat.checkCatSP2(selectP2);
    sleep(const Duration(seconds: 1));
    clearScreen();
  }

  if (mainCat.count1 > 0 && mainCat.count2 > 0) {
    print("------------------------");
    print(" " + mainCat.name1 + " VS " + mainCat.name2);
    print("------------------------");
    sleep(const Duration(seconds: 2));

    print("Player1 -> ${mainCat.name1}");
    checkPic1(mainCat, p1cat1, p1cat2, p1cat3);
    print("ATK: 1-3");
    print("HP: ${mainCat.hp1}");
    print("Mana: ${mainCat.mana1}");
    print("Special: ${mainCat.special1}");
    print("------------------------");
    print("-----------VS-----------");
    print("------------------------");
    print("Player2 -> ${mainCat.name2}");
    checkPic2(mainCat, p2cat1, p2cat2, p2cat3);
    print("ATK: 1-3");
    print("HP: ${mainCat.hp2}");
    print("Mana: ${mainCat.mana2}");
    print("Special: ${mainCat.special2}");
    print("------------------------");
    sleep(const Duration(seconds: 2));

    print("Please enter x to play game");
    String? next = stdin.readLineSync();
    while (true) {
      if (next == "x") {
        break;
      } else {
        print("Please enter x to play game");
        next = stdin.readLineSync();
      }
    }

    print("------------------------");
    print("    Fighting Start!!");
    print("------------------------");
    sleep(const Duration(seconds: 2));
    clearScreen();

    print("------------------------");

    while (mainCat.hp1 > 0 && mainCat.hp2 > 0) {
      if (currentPlayer == "P1") {
        print("Turn: ${mainCat.name1} (Player1)");
        checkPic1(mainCat, p1cat1, p1cat2, p1cat3);
        print("ATK: 1-3");
        print("HP: ${mainCat.hp1}");
        print("Mana: ${mainCat.mana1}");
        print("Special: ${mainCat.special1}");
        print("------------------------");
        print("Player2 HP: ${mainCat.hp2}");
        print("------------------------");
        print("Select your action:");
        print("1)Attack \n2)Heal (Use 2 Mana)");
        print("------------------------");
        stdout.write("Select action: ");
        String? p1Action = stdin.readLineSync();
        switch (p1Action) {
          case "1":
            print("Player1 Attack");
            checkAtk1(mainCat, p1cat1, p1cat2, p1cat3);
            mainCat.mana1 += 1;
            print("------------------------");
            sleep(const Duration(seconds: 2));
            clearScreen();
            break;
          case "2":
            if (mainCat.mana1 >= 2) {
              print("Player1 Heal");
              checkHeal1(mainCat, p1cat1, p1cat2, p1cat3);
              mainCat.mana1 -= 2;
              print("------------------------");
              sleep(const Duration(seconds: 2));
              clearScreen();
              break;
            }
            print("Player1 not enough mana!");
            print("------------------------");
            sleep(const Duration(seconds: 2));
            clearScreen();
            countH = 1;
        }
      } else if (currentPlayer == "P2") {
        print("Turn: ${mainCat.name2} (Player2)");
        checkPic2(mainCat, p2cat1, p2cat2, p2cat3);
        print("ATK: 1-3");
        print("HP: ${mainCat.hp2}");
        print("Mana: ${mainCat.mana2}");
        print("Special: ${mainCat.special2}");
        print("------------------------");
        print("Player1 HP: ${mainCat.hp1}");
        print("------------------------");
        print("Select your action:");
        print("1)Attack \n2)Heal (Use 2 Mana)");
        print("------------------------");
        stdout.write("Select action: ");
        String? p2Action = stdin.readLineSync();
        switch (p2Action) {
          case "1":
            print("Player2 Attack");
            checkAtk2(mainCat, p2cat1, p2cat2, p2cat3);
            mainCat.mana2 += 1;
            print("------------------------");
            sleep(const Duration(seconds: 2));
            clearScreen();
            break;
          case "2":
            if (mainCat.mana2 >= 2) {
              print("Player2 Heal");
              checkHeal2(mainCat, p2cat1, p2cat2, p2cat3);
              mainCat.mana2 -= 2;
              print("------------------------");
              sleep(const Duration(seconds: 2));
              clearScreen();
              break;
            }
            print("Player2 not enough mana!");
            print("------------------------");
            sleep(const Duration(seconds: 2));
            clearScreen();
            countH = 1;
        }
      }
      if (countH == 0) {
        switchPlayer();
      }
      countH = 0;
    }
  }

  if (mainCat.hp1 <= 0) {
    clearScreen();
    print("------------------------");
    print("(Player2)${mainCat.name2} WIN!!");
    checkPic2(mainCat, p2cat1, p2cat2, p2cat3);
    print("------------------------");
  } else if (mainCat.hp2 <= 0) {
    clearScreen();
    print("------------------------");
    print("(Player1)${mainCat.name1} WIN!!");
    checkPic1(mainCat, p1cat1, p1cat2, p1cat3);
    print("------------------------");
  }
}

void checkAtk1(
    MainCat mainCat, whiteCat p1cat1, blackCat p1cat2, giantCat p1cat3) {
  if (mainCat.name1 == p1cat1.getName()) {
    mainCat.hp2 += p1cat1.attack();
  } else if (mainCat.name1 == p1cat2.getName()) {
    mainCat.hp2 += p1cat2.attack();
  } else if (mainCat.name1 == p1cat3.getName()) {
    mainCat.hp2 += p1cat3.attack();
  }
}

void checkAtk2(
    MainCat mainCat, whiteCat p2cat1, blackCat p2cat2, giantCat p2cat3) {
  if (mainCat.name2 == p2cat1.getName()) {
    mainCat.hp1 += p2cat1.attack();
  } else if (mainCat.name2 == p2cat2.getName()) {
    mainCat.hp1 += p2cat2.attack();
  } else if (mainCat.name2 == p2cat3.getName()) {
    mainCat.hp2 += p2cat3.attack();
  }
}

void checkHeal1(
    MainCat mainCat, whiteCat p1cat1, blackCat p1cat2, giantCat p1cat3) {
  if (mainCat.name1 == p1cat1.getName()) {
    mainCat.hp1 += p1cat1.heal();
  } else if (mainCat.name1 == p1cat2.getName()) {
    mainCat.hp1 += p1cat2.heal();
  } else if (mainCat.name1 == p1cat3.getName()) {
    mainCat.hp2 += p1cat3.heal();
  }
}

void checkHeal2(
    MainCat mainCat, whiteCat p2cat1, blackCat p2cat2, giantCat p2cat3) {
  if (mainCat.name2 == p2cat1.getName()) {
    mainCat.hp2 += p2cat1.heal();
  } else if (mainCat.name2 == p2cat2.getName()) {
    mainCat.hp2 += p2cat2.heal();
  } else if (mainCat.name2 == p2cat3.getName()) {
    mainCat.hp2 += p2cat3.heal();
  }
}

void checkPic1(
    MainCat mainCat, whiteCat p1cat1, blackCat p1cat2, giantCat p1cat3) {
  if (mainCat.name1 == p1cat1.getName()) {
    p1cat1.catPic();
  } else if (mainCat.name1 == p1cat2.getName()) {
    p1cat2.catPic();
  } else if (mainCat.name1 == p1cat3.getName()) {
    p1cat3.catPic();
  }
}

void checkPic2(
    MainCat mainCat, whiteCat p2cat1, blackCat p2cat2, giantCat p2cat3) {
  if (mainCat.name2 == p2cat1.getName()) {
    p2cat1.catPic();
  } else if (mainCat.name2 == p2cat2.getName()) {
    p2cat2.catPic();
  } else if (mainCat.name2 == p2cat3.getName()) {
    p2cat3.catPic();
  }
}

void switchPlayer() {
  if (currentPlayer == "P1") {
    currentPlayer = "P2";
  } else {
    currentPlayer = "P1";
  }
}

clearScreen() {
  print("\x1B[2J\x1B[0;0H");
}
