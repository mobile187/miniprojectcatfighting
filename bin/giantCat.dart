import 'cat.dart';
import 'dart:math';

class giantCat extends cat {
  giantCat() : super("Giant Cat", 3, 10, 2);

  @override
  int attack() {
    var rng = Random().nextInt(6);
    if (rng == 0) {
      print("Miss!!");
      return 0;
    } else if (rng == 1 || rng == 2) {
      print("Damage: 1");
      return -1;
    } else if (rng == 3 || rng == 4) {
      print("Damage: 2");
      return -2;
    } else if (rng == 5) {
      print("Damage: 3");
      return -3;
    }
    return 0;
  }

  @override
  int heal() {
    var rng = Random().nextInt(6);
    if (rng == 0 || rng == 1 || rng == 2) {
      //Special
      return special();
    } else if (rng == 3 || rng == 4 || rng == 5) {
      print("Heal: 3");
      return 3;
    }
    return 0;
  }

  @override
  int special() {
    var bigHeal = Random().nextInt(2);
    if (bigHeal == 0) {
      print("Heal: 2");
      return 2;
    } else {
      print("Big Heal!!: 5");
      return 5;
    }
  }

  @override
  String getName() {
    return name;
  }

  @override
  int getAtk() {
    return atk;
  }

  @override
  int getHp() {
    return hp;
  }

  @override
  void setHp(int num) {
    hp = hp + num;
  }

  @override
  int getMana() {
    return mana;
  }

  @override
  void setMana(int num) {
    mana = mana + num;
  }

  @override
  String getSpecial() {
    return "Big Heal!!";
  }

  @override
  void catPic() {
    print("\u001b[41;1m");
    print("       /\\_____/\\        ");
    print("      (  =^o^=  )       ");
    print("\u001b[0m");
  }
}
