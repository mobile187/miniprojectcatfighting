import 'cat.dart';
import 'dart:math';

class blackCat extends cat {
  blackCat() : super("Black Cat", 3, 10, 1);

  @override
  int attack() {
    var rng = Random().nextInt(6);
    if (rng == 0) {
      //Special
      return special();
    } else if (rng == 1 || rng == 2) {
      print("Damage: 1");
      return -1;
    } else if (rng == 3 || rng == 4) {
      print("Damage: 2");
      return -2;
    } else if (rng == 5) {
      print("Damage: 3");
      return -3;
    }
    return 0;
  }

  @override
  int heal() {
    var rng = Random().nextInt(6);
    if (rng == 0 || rng == 1 || rng == 2) {
      print("Heal: 2");
      return 2;
    } else if (rng == 3 || rng == 4 || rng == 5) {
      print("Heal: 3");
      return 3;
    }
    return 0;
  }

  @override
  int special() {
    var noMiss = Random().nextInt(2);
    if (noMiss == 0) {
      print("Never Miss!!: 2");
      return -2;
    } else {
      print("Never Miss!!: 3");
      return -3;
    }
  }

  @override
  String getName() {
    return name;
  }

  @override
  int getAtk() {
    return atk;
  }

  @override
  int getHp() {
    return hp;
  }

  @override
  void setHp(int num) {
    hp = hp + num;
  }

  @override
  int getMana() {
    return mana;
  }

  @override
  void setMana(int num) {
    mana = mana + num;
  }

  @override
  String getSpecial() {
    return "Never Miss!!";
  }

  @override
  void catPic() {
    print("\u001b[40;1m");
    print("        /\\___/\\         ");
    print("       ( ='.'= )         ");
    print("\u001b[0m");
  }
}
