class cat {
  String name;
  int atk;
  int hp;
  int mana;
  cat(this.name, this.atk, this.hp, this.mana);
  void attack() {}
  void heal() {}
  void special() {}

  String getName() {
    return name;
  }

  int getAtk() {
    return atk;
  }

  int getHp() {
    return hp;
  }

  void setHp(int num) {
    hp = hp + num;
  }

  int getMana() {
    return mana;
  }

  void setMana(int num) {
    mana = mana + num;
  }

  String getSpecial() {
    return "";
  }

  void catPic() {}
}
