import 'cat.dart';
import 'dart:math';

class whiteCat extends cat {
  whiteCat() : super("White Cat", 3, 8, 1);

  @override
  int attack() {
    var rng = Random().nextInt(6);
    if (rng == 0) {
      print("Miss!!");
      return 0;
    } else if (rng == 1 || rng == 2) {
      print("Damage: 1");
      return -1;
    } else if (rng == 3 || rng == 4) {
      //Special
      return special();
    } else if (rng == 5) {
      print("Damage: 3");
      return -3;
    }
    return 0;
  }

  @override
  int heal() {
    var rng = Random().nextInt(6);
    if (rng == 0 || rng == 1 || rng == 2) {
      print("Heal: 2");
      return 2;
    } else if (rng == 3 || rng == 4 || rng == 5) {
      print("Heal: 3");
      return 3;
    }
    return 0;
  }

  @override
  int special() {
    var criRate = Random().nextInt(2);
    if (criRate == 0) {
      print("Damage: 2");
      return -2;
    } else {
      print("Critical Damage!!: 4");
      return -4;
    }
  }

  @override
  String getName() {
    return name;
  }

  @override
  int getAtk() {
    return atk;
  }

  @override
  int getHp() {
    return hp;
  }

  @override
  void setHp(int num) {
    hp = hp + num;
  }

  @override
  int getMana() {
    return mana;
  }

  @override
  void setMana(int num) {
    mana = mana + num;
  }

  @override
  String getSpecial() {
    return "Critical Damage!!";
  }

  @override
  void catPic() {
    print("\u001b[47;1m");
    print("        /\\___/\\         ");
    print("       ( =^.^= )        ");
    print("\u001b[0m");
  }
}
